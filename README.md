# Hockey Pongscii
© Marc Cerutti and Tristan Réamot

C++ Project - Windows ACII Console Game - M1 Master JMIN 2021

https://cours.guillaumelevieux.xyz/?module=seances&seance=14

## Controls

Player 1 :
- **ZQSD** for moving
- **F** for creating walls

Player 2 :
- **Arrows** for moving
- **Right Ctrl** for creating walls

Escape to quit, R to restart

## Dependencies
- Cmake (>= 3.XX)
- Eigen3 (included)
- Doxygen (for documentation)
- Clang (for formating code)

## Installation

1. Clone the repository
2. Use Cmake-Gui to make a visual studio solution (configure and generate)
3. Build and enjoy

## Architecture

The poject is articulated around three principles :
- All objects used inside the game are GameElements, that are composed of components (Graphic, Physic, and Collider).
- All components of an object can acess to others attached to this object.
- The Game class is responsible to allocation, initialization and destruction of objects. It is also responsible of the game loop(input, physic, logic, render) and global events (restart, music).

For more details about interactions and class graphes, see [./doc/html/index.html](./doc/html/index.html) in a navigator.