#pragma once

#include <ScreenConstants.h>
#include <Windows.h>
#include <stdio.h>

#include <iostream>

class GameWindow
{
 public:
  GameWindow();
  ~GameWindow();

  void clear();
  void setPixelBuffer( int x, int y, char ascii, int color );
  void render();

 private:
  HANDLE hOutput;

  COORD      dwBufferSize = { SCREEN_WIDTH, SCREEN_HEIGHT };
  COORD      dwBufferCoord = { 0, 0 };
  SMALL_RECT rcRegion = { 0, 0, SCREEN_WIDTH - 1, SCREEN_HEIGHT - 1 };

  CHAR_INFO buffer[SCREEN_HEIGHT][SCREEN_WIDTH];
};
