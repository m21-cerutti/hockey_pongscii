#pragma once

enum COLORS
{
  DEFAULT = 0x0F,
  COLLIDER_COLOR = 30,
  BALL = 0x005F,
  PLAYER_1 = 0x0042,
  PLAYER_2 = 0x0015,
};
