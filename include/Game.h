#pragma once

#include <GameElements/GameElement.h>
#include <GameElements/Player.h>
#include <stdlib.h>

#include <vector>

class Game
{
 public:
  ~Game();

  static Game& getInstance();
  void         mainLoop();
  void         restartRound( Player* playerWhoScore );

 private:
  Game();
  void createWallBonus( Player* player );
  void restartMatch();
  void checkEndOfGame();
  void checkAliveElements();
  void checkInputs();
  void physicStep();
  void playMusicBeginMatch();
  void playMusicEndMatch();

  // Put as a reference from Main
  GameWindow window;

  // Getter and setter
  std::vector<std::unique_ptr<GameElement>> _elements;
  // Shortcuts system with conversion to access
  int _indexPuck;
  int _indexUi;
  int _indexBonusWalls;

  // Global parameter
  int _nbRound;

  bool _waitStart;
  bool _gameFinished;
  bool _threadActive = false;

  float _deltaTime;  // in s

  Player* _player1;
  Player* _player2;
};
