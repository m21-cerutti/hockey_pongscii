#pragma once

#include <Components/GraphicComponent.h>

class RectGraphic : public GraphicComponent
{
 public:
  explicit RectGraphic( GameElement *element );

  void display( GameWindow &win ) override;
  int  getHeight() const;
  void setHeight( int height );
  int  getWidth() const;
  void setWidth( int width );

 private:
  int _width, _height;
};
