#pragma once

#include <Components/GraphicComponent.h>

class Disk : public GraphicComponent
{
 public:
  explicit Disk( GameElement *element );

  void display( GameWindow &win ) override;
  int  getRadius() const;
  void setRadius( int radius );

 private:
  int _radius;
};
