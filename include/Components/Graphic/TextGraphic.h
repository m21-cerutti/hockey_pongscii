#pragma once

#include <Components/GraphicComponent.h>

#include <eigen3/Eigen/Dense>

class TextGraphic : public GraphicComponent
{
 public:
  explicit TextGraphic( GameElement *element );

  void display( GameWindow &win ) override;

  const Eigen::Vector2f &getOffset() const;
  void                   setOffset( const Eigen::Vector2f &offset );

  const std::string &getText() const;
  void               setText( const std::string &text );

  bool isOutline() const;
  void setOutline( bool outline );

 private:
  Eigen::Vector2f _offset;
  std::string     _text;
  bool            _outline;
};
