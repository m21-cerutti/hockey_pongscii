#pragma once

#include <Components/Graphic/TextGraphic.h>
#include <Components/GraphicComponent.h>
#include <GameElements/Player.h>

#include <vector>

class ScoreBoardGraphic : public GraphicComponent
{
 public:
  explicit ScoreBoardGraphic( GameElement *element, int ScoreBoardHeight );
  ~ScoreBoardGraphic();

  void display( GameWindow &win ) override;
  void setScore( Player *p1, Player *p2 );
  void setMessage( const std::string &text );

 private:
  std::vector<TextGraphic *> parts;
};
