#pragma once

#include <Components/Collider.h>
#include <PhysicsConstants.h>

#include <unordered_set>

enum class PhysicMode
{
  Free,
  Static,
  Constrained,
  Trigger,
  None
};

// Tag vs layer ?
enum Layers
{
  LAYER_0 = 0b0001,
  LAYER_1 = 0b0010,
  LAYER_2 = 0b0100,
  LAYER_3 = 0b1000,
  LAYER_A = 0b1111
};

// TODO Doc
// Transform/Rigidbody
class PhysicComponent : public GameComponent
{
 public:
  explicit PhysicComponent( GameElement* element );

  void computePosition( float deltaTime );

  void collideWith( const PhysicComponent* other );

  void                   setPosition( const Eigen::Vector2f& position );
  const Eigen::Vector2f& getPosition() const;

  void                   setVelocity( const Eigen::Vector2f& velocity );
  const Eigen::Vector2f& getVelocity() const;

  void                   setForce( const Eigen::Vector2f& velocity );
  const Eigen::Vector2f& getForce() const;

  PhysicMode getMode() const;
  void       setMode( PhysicMode mode );

  int  getLayer() const;
  void setLayer( int layer );

  void resetCollisions();

 protected:
  // Nb of object with id for collision ? id = num bit ?
  std::unordered_set<const PhysicComponent*> _inCollisionSet;
  PhysicMode                                 _mode;
  int                                        _layer;

  Eigen::Vector2f _position;
  Eigen::Vector2f _velocity;
  Eigen::Vector2f _force;
  float           _mass;
};
