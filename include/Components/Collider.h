#pragma once

#include <Components/GameComponent.h>

#include <eigen3/Eigen/Dense>

enum class ColliderType
{
  Circle,
  Box,
  BoxSurround
};

struct Collision
{
  float           penetration;
  Eigen::Vector2f normal;
};

class Collider : public GameComponent
{
 public:
  explicit Collider( GameElement* element );

  bool intersects( const Collider* other, Collision& outCollision );

  Eigen::AlignedBox2f getBox() const;

  ColliderType getType() const;
  void         setType( ColliderType type );

  const Eigen::Vector2f& getHalfSize() const;
  void                   setHalfSize( const Eigen::Vector2f& halfSize );

 private:
  ColliderType    _type;
  Eigen::Vector2f _halfSize;
};
