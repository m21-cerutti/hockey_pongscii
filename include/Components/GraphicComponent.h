#pragma once

#include <Components/GameComponent.h>
#include <GameWindow.h>

#include <Colors.h.>

// TODO DOC
class GraphicComponent : public GameComponent
{
 public:
  explicit GraphicComponent( GameElement* element );

  virtual void display( GameWindow& win );
  void         debugCollider( GameWindow& win );

  int  getColor() const;
  void setColor( int color );

 protected:
  int _color;
};
