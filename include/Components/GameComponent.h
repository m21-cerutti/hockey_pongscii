#pragma once

class GameElement;

// TODO DOC
// Component
class GameComponent
{
 public:
  explicit GameComponent( GameElement* element );

  GameElement& getElement() const;

 protected:
  GameElement* _element;
};
