#pragma once

#include <GameElements/GameElement.h>
#include <GameElements/Player.h>

class ScoreBoard : public GameElement
{
 public:
  ScoreBoard( int ScoreBoardHeight );

  void setScore( Player *p1, Player *p2 );
  void setMessage( const std::string &text );
};
