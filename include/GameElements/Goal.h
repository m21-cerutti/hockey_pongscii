#pragma once

#include <GameElements/GameElement.h>

class Goal : public GameElement
{
 public:
  Goal( int width, int height, Player* playerWhoScore );

  virtual void collideWith( const Collider* other ) override;

 private:
  int     _width, _height;
  Player* _playerWhoScore;
};
