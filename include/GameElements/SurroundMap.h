#pragma once

#include <GameElements/GameElement.h>

class SurroundMap : public GameElement
{
 public:
  SurroundMap( int width, int height );

  virtual void update( float deltaTime ) override;

  int getWidth() const;
  int getHeight() const;

 private:
  int _width, _height;
};