#pragma once

#include <GameElements/GameElement.h>

class Puck : public GameElement
{
 public:
  Puck();
  ~Puck();

  void collideWith( const Collider* other ) override;
};
