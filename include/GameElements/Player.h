#pragma once

#include <GameElements/GameElement.h>

class Player : public GameElement
{
 public:
  Player( int noPlayer );

  // Hérité via GameElement
  virtual void update( float deltaTime ) override;
  virtual void collideWith( const Collider* other ) override;

  void moveUp();
  void moveDown();
  void moveLeft();
  void moveRight();

  float getTimer() const;
  void  resetTimer();

  bool canUseBonusWall() const;
  bool useBonusWall();

  float getSpeed() const;
  void  setSpeed( float speed );

  int  getScore() const;
  void setScore( int score );

  int  getNoPlayer() const;
  void setNoPlayer( int noPlayer );

 private:
  float       _speed = PLAYER_SPEED;
  int         _score = 0;
  const float _max_time_bonus;  // s
  float       _timer;           // s
  int         _noPlayer;
};
