#pragma once

#include <GameElements/GameElement.h>

class Wall : public GameElement
{
 public:
  Wall( int width, int height, bool isDestructible = false );

  virtual void collideWith( const Collider* other ) override;

  bool isIsDestructible() const;
  int  getWidth() const;
  int  getHeight() const;

 private:
  bool _isDestructible;
  int  _width, _height;
};