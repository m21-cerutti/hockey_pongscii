#pragma once

#include <Components/GraphicComponent.h>
#include <Components/PhysicComponent.h>

/**
 * Entity object that is the bridge between several components.
 */
class GameElement
{
 public:
  GameElement( std::string tag );
  GameElement( std::string tag, GraphicComponent* graphic, PhysicComponent* physic, Collider* collider );
  ~GameElement();

  virtual void update( float deltaTime );

  virtual void collideWith( const Collider* other );

  GraphicComponent* getGraphicComponent() const;
  PhysicComponent*  getPhysicComponent() const;
  Collider*         getCollider() const;

  void               setTag( const std::string& tag );
  const std::string& getTag() const;

  bool isAlive() const;
  void setAlive( bool isAlive );

 private:
  std::string       _tag;
  bool              _isAlive;
  Collider*         _collider;
  GraphicComponent* _graphic;
  PhysicComponent*  _physic;
};
