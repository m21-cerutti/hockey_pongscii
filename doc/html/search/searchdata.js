var indexSectionsWithContent =
{
  0: "_bcdfghilmnprstuw~",
  1: "cdgprstw",
  2: "cdgprstw",
  3: "cdgimprstuw~",
  4: "_bdhnprsw",
  5: "clp",
  6: "bcdflnpst",
  7: "_lnpst"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "files",
  3: "functions",
  4: "variables",
  5: "enums",
  6: "enumvalues",
  7: "defines"
};

var indexSectionLabels =
{
  0: "Tout",
  1: "Classes",
  2: "Fichiers",
  3: "Fonctions",
  4: "Variables",
  5: "Énumérations",
  6: "Valeurs énumérées",
  7: "Macros"
};

