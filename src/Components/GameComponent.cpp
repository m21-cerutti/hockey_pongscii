#include <Components/GameComponent.h>

GameComponent::GameComponent( GameElement *element ) : _element( element ) {}

GameElement &GameComponent::getElement() const { return *_element; }
