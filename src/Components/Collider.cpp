#include <Components/Collider.h>
#include <GameElements/GameElement.h>

Collider::Collider( GameElement* element ) : GameComponent( element ), _halfSize( 2, 2 ), _type( ColliderType::Box ) {}

bool Collider::intersects( const Collider* other, Collision& outCollision )
{
  Eigen::Vector2f  position = _element->getPhysicComponent()->getPosition();
  PhysicComponent* otherPhysic = other->_element->getPhysicComponent();
  Eigen::Vector2f  otherPosition = otherPhysic->getPosition();

  Eigen::Vector2f dist = position - otherPosition;

  bool circle = otherPhysic->getElement().getCollider()->_type == ColliderType::Circle || _type == ColliderType::Circle;
  bool box = otherPhysic->getElement().getCollider()->_type == ColliderType::Box || _type == ColliderType::Box;
  bool boxSurround =
      otherPhysic->getElement().getCollider()->_type == ColliderType::BoxSurround || _type == ColliderType::BoxSurround;

  if ( circle && boxSurround )
  {
    Eigen::AlignedBox2f intersection = getBox().intersection( other->getBox() );

    if ( intersection.isEmpty() )
    {
      Eigen::Vector2f overlap = -dist;
      float           signx = overlap.x() > 0 ? 1.0f : -1.0f;
      float           signy = overlap.y() > 0 ? 1.0f : -1.0f;
      overlap = Eigen::Vector2f( signx * max( abs( overlap.x() ) - other->getHalfSize().x(), 0 ),
                                 signy * max( abs( overlap.y() ) - other->getHalfSize().y(), 0 ) );

      outCollision.normal = overlap.normalized();
      outCollision.penetration = overlap.norm();
      return true;
    }
    return false;
  }
  else if ( circle && box )
  {
    Eigen::AlignedBox2f intersection = getBox().intersection( other->getBox() );

    if ( intersection.isEmpty() ) return false;

    Eigen::Vector2f overlap = position - intersection.center();

    float x_penetration = abs( overlap.x() );
    float y_penetration = abs( overlap.y() );
    if ( x_penetration > y_penetration )
    {
      outCollision.normal = Eigen::Vector2f( overlap.x(), 0.0f ).normalized();
      outCollision.penetration = intersection.sizes()[0];
    }
    else
    {
      outCollision.normal = Eigen::Vector2f( 0.0f, overlap.y() ).normalized();
      outCollision.penetration = intersection.sizes()[1];
    }
    return true;
  }
  else if ( box )
  {
    /* NOT USED*/
    return false;
  }
  else if ( circle )
  {
    outCollision.normal = dist.normalized();
    outCollision.penetration = _halfSize.norm();
    return ( dist ).norm() < ( otherPhysic->getElement().getCollider()->getHalfSize().norm() + _halfSize.norm() );
  }
  return false;
}

Eigen::AlignedBox2f Collider::getBox() const
{
  const Eigen::Vector2f& position = _element->getPhysicComponent()->getPosition();
  return Eigen::AlignedBox2f( position - _halfSize, position + _halfSize );
}

ColliderType           Collider::getType() const { return _type; }
void                   Collider::setType( ColliderType type ) { _type = type; }
const Eigen::Vector2f& Collider::getHalfSize() const { return _halfSize; }
void                   Collider::setHalfSize( const Eigen::Vector2f& halfSize ) { _halfSize = halfSize; }
