#include <Components/PhysicComponent.h>
#include <GameElements/GameElement.h>

PhysicComponent::PhysicComponent( GameElement* element )
    : GameComponent( element ),
      _position( 0, 0 ),
      _velocity( 0.0f, 0.0f ),
      _mass( 1.0f ),
      _force( 0.0f, 0.0f ),
      _mode( PhysicMode::Static ),
      _layer( Layers::LAYER_0 )
{
}

void PhysicComponent::setPosition( const Eigen::Vector2f& position ) { _position = position; }

void PhysicComponent::computePosition( float deltaTime )
{
  if ( _mode != PhysicMode::Static && _mode != PhysicMode::Trigger && _mode != PhysicMode::None )
  {
    if ( _velocity.norm() > LIMIT_SPEED ) _velocity = _velocity.normalized() * LIMIT_SPEED;
    _position += _velocity * deltaTime;
    if ( _mode == PhysicMode::Free )
    {
      Eigen::Vector2f acc = _force / _mass;
      _velocity += acc * deltaTime;
    }
  }
}

void PhysicComponent::collideWith( const PhysicComponent* other )
{
  // No physics between statics and constrained
  if ( ( _mode == PhysicMode::Static && other->_mode == PhysicMode::Static ) ||
       ( other->_mode == PhysicMode::Constrained && _mode == PhysicMode::Constrained ) || _mode == PhysicMode::None ||
       other->_mode == PhysicMode::None )
    return;

  Collider* collider = getElement().getCollider();
  Collider* otherCollider = other->getElement().getCollider();
  Collision collision;

  // Collision test
  bool intersected = collider->intersects( otherCollider, collision );
  bool triggerCollision = _mode == PhysicMode::Trigger || other->_mode == PhysicMode::Trigger;

  // Layer test
  if ( ( other->_layer & _layer ) != 0 )
  {
    std::unordered_set<const PhysicComponent*>::iterator it;
    bool alreadyCollided = ( it = _inCollisionSet.find( other ) ) != _inCollisionSet.cend();

    if ( !intersected && alreadyCollided )
    {
      _inCollisionSet.erase( it );
    }
    else if ( intersected && !alreadyCollided )
    {
      if ( _mode != PhysicMode::Static && !triggerCollision )  // To handle logic collision in statics but no physics
      {
        // Physic collision response
        Eigen::Vector2f penetration = collision.normal * collision.penetration;
        _position += penetration;

        if ( _mode == PhysicMode ::Free )
        {
          // Physic free (ball)
          Eigen::Vector2f t = collision.normal.unitOrthogonal();
          Eigen::Vector2f vn = collision.normal.dot( _velocity ) * collision.normal;
          Eigen::Vector2f vt = t.dot( _velocity ) * t;

          float colinearVelocity = -1;
          if ( _velocity.norm() > 0.1f )
            colinearVelocity = _velocity.normalized().dot( collision.normal ) >= 0 ? 1.2f : -1.0f;
          _velocity = vt + colinearVelocity * vn;

          if ( other->_velocity.norm() > 0.1f )
          {
            Eigen::Vector2f speedImpact = collision.normal.dot( other->_velocity ) * collision.normal;
            Eigen::Vector2f effect = t.dot( other->_velocity ) * t;
            _velocity += speedImpact.normalized() * SPEED_INCREMENT;
            _force = effect;
          }
          else
          {
            _force = 0.5f * _force;
          }
        }
      }

      _inCollisionSet.insert( other );
      _element->collideWith( otherCollider );
    }
    else if ( intersected && _mode != PhysicMode::Static && !triggerCollision )
    {
      Eigen::Vector2f penetration = collision.normal * collision.penetration;
      _position += penetration;
    }
  }
}

const Eigen::Vector2f& PhysicComponent::getPosition() const { return _position; }
void                   PhysicComponent::setVelocity( const Eigen::Vector2f& velocity ) { _velocity = velocity; }
const Eigen::Vector2f& PhysicComponent::getVelocity() const { return _velocity; }
void                   PhysicComponent::setForce( const Eigen::Vector2f& force ) { _force = force; }
const Eigen::Vector2f& PhysicComponent::getForce() const { return _force; }
PhysicMode             PhysicComponent::getMode() const { return _mode; }
void                   PhysicComponent::setMode( PhysicMode mode ) { _mode = mode; }
int                    PhysicComponent::getLayer() const { return _layer; }
void                   PhysicComponent::setLayer( int layer ) { _layer = layer; }
void                   PhysicComponent::resetCollisions() { _inCollisionSet.clear(); }
