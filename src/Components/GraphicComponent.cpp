#include <Components/GraphicComponent.h>
#include <Components/PhysicComponent.h>
#include <GameElements/GameElement.h>

GraphicComponent::GraphicComponent( GameElement* element ) : GameComponent( element ), _color( COLORS::DEFAULT ) {}

void GraphicComponent::display( GameWindow& win ) { debugCollider( win ); }

void GraphicComponent::debugCollider( GameWindow& win )
{
  const PhysicComponent* physic = _element->getPhysicComponent();
  const Eigen::Vector2f& pos = physic->getPosition();

  win.setPixelBuffer( (int)( pos.x() ), (int)( pos.y() ), '0', COLORS::COLLIDER_COLOR );
  Eigen::AlignedBox2f    box = getElement().getCollider()->getBox();
  const Eigen::Vector2f& p1 = box.corner( Eigen::AlignedBox2f::BottomLeft );
  const Eigen::Vector2f& p2 = box.corner( Eigen::AlignedBox2f::TopRight );
  win.setPixelBuffer( (int)( p1.x() ), (int)( p1.y() ), 'L', COLORS::COLLIDER_COLOR );
  win.setPixelBuffer( (int)( p2.x() ), (int)( p2.y() ), 'R', COLORS::COLLIDER_COLOR );
}
int  GraphicComponent::getColor() const { return _color; }
void GraphicComponent::setColor( int color ) { _color = color; }
