#include <Components/Graphic/ScoreBoardGraphic.h>

ScoreBoardGraphic::ScoreBoardGraphic( GameElement *element, int ScoreBoardHeight ) : GraphicComponent( element )
{
  TextGraphic *p1 = new TextGraphic( element );
  p1->setOffset( Eigen::Vector2f( SCREEN_WIDTH * 0.25, ScoreBoardHeight / 2.0f ) );
  p1->setColor( 4 );
  parts.push_back( p1 );

  TextGraphic *p2 = new TextGraphic( element );
  p2->setOffset( Eigen::Vector2f( SCREEN_WIDTH * 0.75, ScoreBoardHeight / 2.0f ) );
  p2->setColor( 9 );
  parts.push_back( p2 );

  TextGraphic *message = new TextGraphic( element );
  message->setOffset( Eigen::Vector2f( SCREEN_WIDTH * 0.5f, SCREEN_HEIGHT * 0.5f ) );
  parts.push_back( message );

  parts[0]->setText( " Player 1 : 0 " );
  parts[1]->setText( " Player 2 : 0 " );
  parts[2]->setOutline( true );
}

ScoreBoardGraphic::~ScoreBoardGraphic()
{
  for ( auto &text : parts )
  {
    delete text;
  }
}

void ScoreBoardGraphic::display( GameWindow &win )
{
  for ( auto &text : parts )
  {
    text->display( win );
  }
}

void ScoreBoardGraphic::setScore( Player *p1, Player *p2 )
{
  parts[0]->setText( " Player 1 : " + std::to_string( p1->getScore() ) + " " + ( p1->canUseBonusWall() ? "X " : "" ) );
  parts[1]->setText( " Player 2 : " + std::to_string( p2->getScore() ) + " " + ( p2->canUseBonusWall() ? "X " : "" ) );
}

void ScoreBoardGraphic::setMessage( const std::string &text ) { parts[2]->setText( text ); }
