#include <Components/Graphic/RectGraphic.h>
#include <Components/PhysicComponent.h>
#include <GameElements/GameElement.h>

RectGraphic::RectGraphic( GameElement *element ) : GraphicComponent( element ), _width( 1 ), _height( 1 ) {}

void RectGraphic::display( GameWindow &win )
{
  const PhysicComponent *physic = _element->getPhysicComponent();
  const Eigen::Vector2f &pos = physic->getPosition();

  for ( float i = -( _width / 2.0f ); i <= _width / 2.0f; i++ )
  {
    for ( float j = -( _height / 2.0f ); j <= _height / 2.0f; j++ )
    {
      win.setPixelBuffer( (int)( pos.x() + i ), (int)( pos.y() + j ), 'X', _color );
    }
  }
}
int  RectGraphic::getHeight() const { return _height; }
void RectGraphic::setHeight( int height ) { _height = height; }
int  RectGraphic::getWidth() const { return _width; }
void RectGraphic::setWidth( int width ) { _width = width; }
