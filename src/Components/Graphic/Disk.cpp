#include <Components/Graphic/Disk.h>
#include <Components/PhysicComponent.h>
#include <GameElements/GameElement.h>

#define _USE_MATH_DEFINES
#include <math.h>

Disk::Disk( GameElement *element ) : GraphicComponent( element ), _radius( 2 ) {}

void Disk::display( GameWindow &win )
{
  const PhysicComponent *physic = _element->getPhysicComponent();
  const Eigen::Vector2f &pos = physic->getPosition();

  for ( int y = -_radius; y < _radius; y++ )
  {
    int half_row_width = (int)( sqrt( _radius * _radius - y * y ) );
    for ( int x = -half_row_width; x < half_row_width; x++ )
    {
      int x_pix = (int)( pos.x() + x );
      int y_pix = (int)( pos.y() + y );
      win.setPixelBuffer( x_pix, y_pix, '.', _color );
    }
  }
}
int  Disk::getRadius() const { return _radius; }
void Disk::setRadius( int radius ) { _radius = radius; }
