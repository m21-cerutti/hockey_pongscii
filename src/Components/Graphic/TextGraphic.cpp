#include <Components/Graphic/TextGraphic.h>
#include <Components/PhysicComponent.h>
#include <GameElements/GameElement.h>

TextGraphic::TextGraphic( GameElement *element ) : GraphicComponent( element ), _offset( 0.0f, 0.0f ), _outline( false )
{
}

void TextGraphic::display( GameWindow &win )
{
  if ( !_text.empty() )
  {
    const PhysicComponent *physic = _element->getPhysicComponent();
    const Eigen::Vector2f &pos = physic->getPosition();

    const char *buffText = _text.c_str();

    int offset = (int)( pos.x() + _offset.x() - _text.size() / 2.0f );
    for ( int i = 0; i < _text.size(); i++ )
    {
      win.setPixelBuffer( (int)( offset + i ), (int)( pos.y() + _offset.y() ), buffText[i], _color );
    }
    if ( _outline )
    {
      win.setPixelBuffer( (int)( pos.x() + offset - 1 ), (int)( pos.y() + _offset.y() ), ' ', _color );
      win.setPixelBuffer( (int)( pos.x() + offset + _text.size() + 1 ), (int)( pos.y() + _offset.y() ), ' ', _color );
      for ( int i = 0; i < _text.size(); i++ )
      {
        win.setPixelBuffer( offset + i, pos.y() + _offset.y() + 1, ' ', _color );
      }
      for ( int i = 0; i < _text.size(); i++ )
      {
        win.setPixelBuffer( offset + i, pos.y() + _offset.y() - 1, ' ', _color );
      }
    }
  }
}
const Eigen::Vector2f &TextGraphic::getOffset() const { return _offset; }
void                   TextGraphic::setOffset( const Eigen::Vector2f &offset ) { _offset = offset; }
const std::string     &TextGraphic::getText() const { return _text; }
void                   TextGraphic::setText( const std::string &text ) { _text = text; }
bool                   TextGraphic::isOutline() const { return _outline; }
void                   TextGraphic::setOutline( bool outline ) { _outline = outline; }
