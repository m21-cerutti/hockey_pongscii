#include <Components/Graphic/RectGraphic.h>
#include <Components/PhysicComponent.h>
#include <Game.h>
#include <GameElements/Goal.h>

Goal::Goal( int width, int height, Player* playerWhoScore )
    : GameElement( "Goal", nullptr, new PhysicComponent( this ), nullptr ),
      _width( width ),
      _height( height ),
      _playerWhoScore( playerWhoScore )
{
  getPhysicComponent()->setMode( PhysicMode::Trigger );

  Collider* collider = getCollider();
  collider->setType( ColliderType::Box );
  collider->setHalfSize( Eigen::Vector2f( _width / 2.0f + 1.0f, _height / 2.0f + 1.0f ) );
}

void Goal::collideWith( const Collider* other )
{
  if ( other->getElement().getTag() == "Puck" )
  {
    _playerWhoScore->setScore( _playerWhoScore->getScore() + 1 );
    Game::getInstance().restartRound( _playerWhoScore );
  }
}
