#include <Components/PhysicComponent.h>
#include <GameElements/SurroundMap.h>

SurroundMap::SurroundMap( int width, int height )
    : GameElement( "Surround", new GraphicComponent( this ), new PhysicComponent( this ), nullptr ),
      _width( width ),
      _height( height )
{
  getPhysicComponent()->setMode( PhysicMode::Static );

  Collider* collider = getCollider();
  collider->setType( ColliderType::BoxSurround );
  collider->setHalfSize( Eigen::Vector2f( _width / 2.0f, _height / 2.0f - 3.0f ) );
}

void SurroundMap::update( float deltaTime ) {}
int  SurroundMap::getWidth() const { return _width; }
int  SurroundMap::getHeight() const { return _height; }
