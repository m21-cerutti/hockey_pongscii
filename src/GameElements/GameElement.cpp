#include <GameElements/GameElement.h>

#include <utility>

GameElement::GameElement( std::string tag ) : GameElement( std::move( tag ), nullptr, nullptr, nullptr ) {}

GameElement::GameElement( std::string tag, GraphicComponent* graphic, PhysicComponent* physic, Collider* collider )
    : _tag( std::move( std::move( tag ) ) ),
      _graphic( graphic ),
      _physic( physic ),
      _collider( collider ),
      _isAlive( true )
{
  if ( _graphic == nullptr )
  {
    _graphic = new GraphicComponent( this );
  }
  if ( _physic == nullptr )
  {
    _physic = new PhysicComponent( this );
  }
  if ( _collider == nullptr )
  {
    _collider = new Collider( this );
  }
}

GameElement::~GameElement()
{
  delete _graphic;
  delete _physic;
  delete _collider;
}

void GameElement::update( float deltaTime ) {}

void GameElement::collideWith( const Collider* other ) {}

GraphicComponent*  GameElement::getGraphicComponent() const { return _graphic; }
PhysicComponent*   GameElement::getPhysicComponent() const { return _physic; }
Collider*          GameElement::getCollider() const { return _collider; }
const std::string& GameElement::getTag() const { return _tag; }
void               GameElement::setTag( const std::string& tag ) { _tag = tag; }
bool               GameElement::isAlive() const { return _isAlive; }
void               GameElement::setAlive( bool isAlive ) { _isAlive = isAlive; }
