#include <Components/Graphic/RectGraphic.h>
#include <Components/PhysicComponent.h>
#include <GameElements/Wall.h>

Wall::Wall( int width, int height, bool isDestructible )
    : GameElement( "Wall", new RectGraphic( this ), new PhysicComponent( this ), nullptr ),
      _width( width ),
      _height( height ),
      _isDestructible( isDestructible )
{
  getPhysicComponent()->setMode( PhysicMode::Static );

  Collider* collider = getCollider();
  collider->setType( ColliderType::Box );
  collider->setHalfSize( Eigen::Vector2f( _width / 2.0f + 1.0f, _height / 2.0f + 1.0f ) );

  auto* rect = (RectGraphic*)getGraphicComponent();
  rect->setWidth( _width );
  rect->setHeight( _height );
}

void Wall::collideWith( const Collider* other )
{
  if ( _isDestructible && other->getElement().getTag() == "Puck" )
  {
    setAlive( false );
  }
}
bool Wall::isIsDestructible() const { return _isDestructible; }
int  Wall::getWidth() const { return _width; }
int  Wall::getHeight() const { return _height; }
