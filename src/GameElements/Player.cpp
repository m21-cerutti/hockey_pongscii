#include <Components/Graphic/Disk.h>
#include <Components/PhysicComponent.h>
#include <GameElements/Player.h>
#include <GameplayConstants.h>

Player::Player( int noPlayer )
    : GameElement( "Player", new Disk( this ), new PhysicComponent( this ), nullptr ),
      _max_time_bonus( TIMER_WALL_BONUS ),
      _timer( 0 ),
      _noPlayer( noPlayer )
{
  Collider* collider = getCollider();
  collider->setHalfSize( Eigen::Vector2f( 2.5f, 2.5f ) );
  collider->setType( ColliderType::Circle );

  getPhysicComponent()->setMode( PhysicMode::Constrained );

  Disk* graphic = (Disk*)getGraphicComponent();
  ( (Disk*)getGraphicComponent() )->setRadius( 4 );
  if ( noPlayer == 1 )
  {
    graphic->setColor( COLORS::PLAYER_1 );
  }
  else
  {
    graphic->setColor( COLORS::PLAYER_2 );
  }
}

void Player::update( float deltaTime )
{
  getPhysicComponent()->setVelocity( Eigen::Vector2f( 0, 0 ) );
  if ( _timer <= _max_time_bonus )
  {
    _timer += deltaTime;
  }
}

void Player::collideWith( const Collider* other ) { getPhysicComponent()->setVelocity( Eigen::Vector2f( 0, 0 ) ); }

void Player::moveUp()
{
  getPhysicComponent()->setVelocity( getPhysicComponent()->getVelocity() + Eigen::Vector2f( 0, -_speed ) );
}

void Player::moveDown()
{
  getPhysicComponent()->setVelocity( getPhysicComponent()->getVelocity() + Eigen::Vector2f( 0, _speed ) );
}

void Player::moveLeft()
{
  getPhysicComponent()->setVelocity( getPhysicComponent()->getVelocity() + Eigen::Vector2f( -_speed, 0 ) );
}

void Player::moveRight()
{
  getPhysicComponent()->setVelocity( getPhysicComponent()->getVelocity() + Eigen::Vector2f( _speed, 0 ) );
}

void Player::resetTimer() { _timer = 0; }

bool Player::canUseBonusWall() const { return _timer > _max_time_bonus; }

bool Player::useBonusWall()
{
  if ( _timer > _max_time_bonus )
  {
    _timer = 0;
    return true;
  }
  return false;
}
float Player::getSpeed() const { return _speed; }
void  Player::setSpeed( float speed ) { _speed = speed; }
int   Player::getScore() const { return _score; }
void  Player::setScore( int score ) { _score = score; }
float Player::getTimer() const { return _timer; }
int   Player::getNoPlayer() const { return _noPlayer; }
void  Player::setNoPlayer( int noPlayer ) { _noPlayer = noPlayer; }
