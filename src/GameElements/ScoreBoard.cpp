#include <Components/Graphic/ScoreBoardGraphic.h>
#include <GameElements/ScoreBoard.h>

ScoreBoard::ScoreBoard( int ScoreBoardHeight )
    : GameElement( "ScoreBoard", new ScoreBoardGraphic( this, ScoreBoardHeight ), new PhysicComponent( this ), nullptr )
{
  getPhysicComponent()->setMode( PhysicMode::None );
}

void ScoreBoard::setScore( Player *p1, Player *p2 )
{
  ( (ScoreBoardGraphic *)getGraphicComponent() )->setScore( p1, p2 );
}

void ScoreBoard::setMessage( const std::string &text )
{
  ( (ScoreBoardGraphic *)getGraphicComponent() )->setMessage( text );
}
