#include <Components/Graphic/Disk.h>
#include <Components/PhysicComponent.h>
#include <GameElements/Puck.h>

#include <thread>

Puck::Puck() : GameElement( "Puck", new Disk( this ), new PhysicComponent( this ), nullptr )
{
  Disk* graphic = (Disk*)getGraphicComponent();
  graphic->setRadius( 2 );
  graphic->setColor( COLORS::BALL );

  Collider* collider = getCollider();
  collider->setHalfSize( Eigen::Vector2f( 2.0f, 2.0f ) );
  collider->setType( ColliderType::Circle );

  PhysicComponent* physic = getPhysicComponent();
  physic->setMode( PhysicMode::Free );
}

Puck::~Puck() = default;

void Puck::collideWith( const Collider* other )
{
  if ( other->getElement().getTag() != "Goal" )
  {
    std::thread sfx(
        [&]()
        {
          float ratioSpeed = ( getPhysicComponent()->getVelocity().norm() / LIMIT_SPEED );
          float duration = 200 - 150 * ratioSpeed;
          float freq = 200 + ratioSpeed * 420;
          Beep( freq, duration );
        } );
    sfx.detach();
  }
}
