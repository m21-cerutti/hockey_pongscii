#include <GameWindow.h>

#include <Colors.h.>

GameWindow::GameWindow()
{
  SetLastError( NO_ERROR );

  // Buffer
  dwBufferSize = { SCREEN_WIDTH, SCREEN_HEIGHT };
  dwBufferCoord = { 0, 0 };
  rcRegion = { 0, 0, SCREEN_WIDTH - 1, SCREEN_HEIGHT - 1 };

  hOutput = GetStdHandle( STD_OUTPUT_HANDLE );
  HWND hwnd_console = GetConsoleWindow();
  SetWindowLongPtr( hwnd_console, GWL_STYLE, ( WS_OVERLAPPED | WS_CAPTION | WS_SYSMENU | WS_MINIMIZEBOX ) );

  CONSOLE_SCREEN_BUFFER_INFO info;
  GetConsoleScreenBufferInfo( hOutput, &info );

  // Size

  RECT rect;
  GetWindowRect( hwnd_console, &rect );
  int char_height = ( rect.bottom - rect.top ) / ( info.srWindow.Bottom - info.srWindow.Top );
  int char_width = ( rect.right - rect.left ) / ( info.srWindow.Right - info.srWindow.Left );


  COORD new_size;
  new_size.X = ( info.srWindow.Right - info.srWindow.Left ) + 1;
  new_size.Y = ( info.srWindow.Bottom - info.srWindow.Top ) + 1;

  SetConsoleScreenBufferSize( hOutput, new_size );

  // Deactivate Scroll
  SetWindowPos( hwnd_console, nullptr, 0, 0, char_width * ( SCREEN_WIDTH + 2 ), char_height * ( SCREEN_HEIGHT ),
                SWP_NOZORDER | SWP_NOMOVE | SWP_NOACTIVATE | SWP_DRAWFRAME );
  

  // Cursor visibility
  CONSOLE_CURSOR_INFO cursorInfo;
  GetConsoleCursorInfo( hOutput, &cursorInfo );
  cursorInfo.bVisible = false;  // set the cursor visibility
  SetConsoleCursorInfo( hOutput, &cursorInfo );

  // Square Font
  CONSOLE_FONT_INFOEX cfi;
  cfi.cbSize = sizeof( cfi );
  cfi.nFont = 10;
  cfi.dwFontSize.X = 12;  // Width of each character in the font
  cfi.dwFontSize.Y = 12;  // Height
  cfi.FontFamily = FF_DONTCARE;
  cfi.FontWeight = FW_NORMAL;
  wcscpy_s( cfi.FaceName, L"Consolas" );  // Choose the font
  SetCurrentConsoleFontEx( hOutput, FALSE, &cfi );

  ShowWindow( hwnd_console, SW_SHOW );
  ReadConsoleOutput( hOutput, (CHAR_INFO *)buffer, dwBufferSize, dwBufferCoord, &rcRegion );
}

GameWindow::~GameWindow() = default;

void GameWindow::clear()
{
  for ( int i = 0; i < SCREEN_WIDTH; i++ )
  {
    for ( auto &j : buffer )
    {
      j[i].Char.AsciiChar = ' ';
      j[i].Attributes = COLORS::DEFAULT;
    }
  }
}

void GameWindow::setPixelBuffer( int x, int y, char ascii, int color )
{
  if ( x >= 0 && y >= 0 && x < SCREEN_WIDTH && y < SCREEN_HEIGHT )
  {
    buffer[y][x].Char.AsciiChar = ascii;
    buffer[y][x].Attributes = color;
  }
}

void GameWindow::render()
{
  WriteConsoleOutput( hOutput, (CHAR_INFO *)buffer, dwBufferSize, dwBufferCoord, &rcRegion );
}
