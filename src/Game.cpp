#include <Game.h>
#include <GameElements/Goal.h>
#include <GameElements/Puck.h>
#include <GameElements/ScoreBoard.h>
#include <GameElements/SurroundMap.h>
#include <GameElements/Wall.h>
#include <GameplayConstants.h>
#include <WinUser.h>

#include <thread>

using namespace std;

Game::Game() : _deltaTime( 0 ), _elements(), window(), _nbRound( NB_ROUNDS )
{
  _deltaTime = 1.0f / 60.0f;
  _gameFinished = false;
  _waitStart = true;
  int painterLayer = 0;

  auto* surround = new SurroundMap( SCREEN_WIDTH + 16.0f, SCREEN_HEIGHT - SCORE_BOARD_HEIGHT - 1 );
  surround->getPhysicComponent()->setPosition(
      Eigen::Vector2f( SCREEN_WIDTH / 2, ( SCREEN_HEIGHT + SCORE_BOARD_HEIGHT ) / 2 ) );
  _elements.push_back( unique_ptr<GameElement>( surround ) );
  painterLayer++;

  Wall* wallTop = new Wall( SCREEN_WIDTH, 2 );
  wallTop->getPhysicComponent()->setPosition( Eigen::Vector2f( SCREEN_WIDTH / 2, SCORE_BOARD_HEIGHT - 1 ) );
  _elements.push_back( unique_ptr<GameElement>( wallTop ) );
  painterLayer++;

  Wall* wallMed = new Wall( 0, SCREEN_HEIGHT - SCORE_BOARD_HEIGHT - 4 );
  wallMed->getPhysicComponent()->setPosition(
      Eigen::Vector2f( SCREEN_WIDTH / 2, SCREEN_HEIGHT / 2 + SCORE_BOARD_HEIGHT / 2 ) );
  wallMed->getPhysicComponent()->setLayer( Layers::LAYER_1 );
  wallMed->getGraphicComponent()->setColor( 0x0008 );
  _elements.push_back( unique_ptr<GameElement>( wallMed ) );
  painterLayer++;

  Wall* wallBot = new Wall( SCREEN_WIDTH, 2 );
  wallBot->getPhysicComponent()->setPosition( Eigen::Vector2f( SCREEN_WIDTH / 2, SCREEN_HEIGHT - 1 ) );
  _elements.push_back( unique_ptr<GameElement>( wallBot ) );
  painterLayer++;

  // Wall Left
  Wall* wallTopLeft = new Wall( 2, SCREEN_HEIGHT * 0.15f );
  wallTopLeft->getPhysicComponent()->setPosition( Eigen::Vector2f( 0, SCREEN_HEIGHT * 0.20f ) );
  _elements.push_back( unique_ptr<GameElement>( wallTopLeft ) );
  painterLayer++;

  Wall* wallBottomLeft = new Wall( 2, SCREEN_HEIGHT * 0.15f );
  wallBottomLeft->getPhysicComponent()->setPosition(
      Eigen::Vector2f( 0, SCREEN_HEIGHT * 0.85f + SCORE_BOARD_HEIGHT / 2.0f ) );
  _elements.push_back( unique_ptr<GameElement>( wallBottomLeft ) );
  painterLayer++;

  // Wall Right
  Wall* wallTopRight = new Wall( 2, SCREEN_HEIGHT * 0.15f );
  wallTopRight->getPhysicComponent()->setPosition( Eigen::Vector2f( SCREEN_WIDTH - 1, SCREEN_HEIGHT * 0.20f ) );
  _elements.push_back( unique_ptr<GameElement>( wallTopRight ) );
  painterLayer++;

  Wall* wallBottomRight = new Wall( 2, SCREEN_HEIGHT * 0.15f );
  wallBottomRight->getPhysicComponent()->setPosition(
      Eigen::Vector2f( SCREEN_WIDTH - 1, SCREEN_HEIGHT * 0.85f + SCORE_BOARD_HEIGHT / 2.0f ) );
  _elements.push_back( unique_ptr<GameElement>( wallBottomRight ) );
  painterLayer++;

  Puck* puck = new Puck();
  puck->getPhysicComponent()->setPosition(
      Eigen::Vector2f( SCREEN_WIDTH / 2, SCREEN_HEIGHT / 2 + SCORE_BOARD_HEIGHT ) );
  _elements.push_back( unique_ptr<GameElement>( puck ) );
  _indexPuck = painterLayer;
  painterLayer++;

  _player1 = new Player( 1 );
  _player1->getPhysicComponent()->setPosition(
      Eigen::Vector2f( SCREEN_WIDTH / 4, SCREEN_HEIGHT / 2 + SCORE_BOARD_HEIGHT ) );
  _player1->getPhysicComponent()->setLayer( Layers::LAYER_0 | Layers::LAYER_1 );
  _elements.push_back( unique_ptr<GameElement>( _player1 ) );
  painterLayer++;

  _player2 = new Player( 2 );
  _player2->getPhysicComponent()->setPosition(
      Eigen::Vector2f( SCREEN_WIDTH * 0.75f, SCREEN_HEIGHT / 2 + SCORE_BOARD_HEIGHT ) );
  _player2->getPhysicComponent()->setLayer( Layers::LAYER_0 | Layers::LAYER_1 );
  _elements.push_back( unique_ptr<GameElement>( _player2 ) );
  painterLayer++;

  // Goals triggers
  Goal* goalP1 = new Goal( 2, 21, _player2 );
  goalP1->getPhysicComponent()->setPosition(
      Eigen::Vector2f( -5.0f, SCREEN_HEIGHT * 0.5f + SCORE_BOARD_HEIGHT / 2 - 1.0f ) );
  _elements.push_back( unique_ptr<GameElement>( goalP1 ) );
  painterLayer++;

  Goal* goalP2 = new Goal( 2, 21, _player1 );
  goalP2->getPhysicComponent()->setPosition(
      Eigen::Vector2f( SCREEN_WIDTH + 5.0f, SCREEN_HEIGHT * 0.5f + SCORE_BOARD_HEIGHT / 2 - 1.0f ) );
  _elements.push_back( unique_ptr<GameElement>( goalP2 ) );
  painterLayer++;

  // UI
  auto* board = new ScoreBoard( SCORE_BOARD_HEIGHT );
  _elements.push_back( unique_ptr<GameElement>( board ) );
  _indexUi = painterLayer;
  painterLayer++;

  _indexBonusWalls = painterLayer;
}

Game::~Game() = default;

void Game::createWallBonus( Player* player )
{
  Wall* wallBonus = new Wall( SIZE_X_WALL_BONUS, SIZE_Y_WALL_BONUS, true );
  wallBonus->getPhysicComponent()->setPosition( player->getPhysicComponent()->getPosition() );
  _elements.push_back( unique_ptr<GameElement>( wallBonus ) );
}

void Game::checkAliveElements()
{
  bool allAlive = true;
  for ( std::unique_ptr<GameElement>& el : _elements )
  {
    allAlive = allAlive && el->isAlive();
  }
  if ( !allAlive )
  {
    _elements.erase( std::remove_if( _elements.begin(), _elements.end(),
                                     []( std::unique_ptr<GameElement>& el ) { return !el->isAlive(); } ),
                     _elements.end() );
  }
}

void Game::checkInputs()
{
  if ( !_gameFinished && !_waitStart )
  {
    if ( GetAsyncKeyState( 0x52 ) ) restartMatch();  // r

    if ( GetAsyncKeyState( 0x5A ) ) _player1->moveUp();     // z
    if ( GetAsyncKeyState( 0x53 ) ) _player1->moveDown();   // s
    if ( GetAsyncKeyState( 0x51 ) ) _player1->moveLeft();   // q
    if ( GetAsyncKeyState( 0x44 ) ) _player1->moveRight();  // d
    if ( GetAsyncKeyState( 0x46 ) )                         // f
    {
      if ( _player1->useBonusWall() )
      {
        createWallBonus( _player1 );
      }
    }

    if ( GetAsyncKeyState( VK_UP ) ) _player2->moveUp();        // up arrow
    if ( GetAsyncKeyState( VK_DOWN ) ) _player2->moveDown();    // down arrow
    if ( GetAsyncKeyState( VK_LEFT ) ) _player2->moveLeft();    // left arrow
    if ( GetAsyncKeyState( VK_RIGHT ) ) _player2->moveRight();  // right arrow
    if ( GetAsyncKeyState( VK_CONTROL ) )                       // control
    {
      if ( _player2->useBonusWall() )
      {
        createWallBonus( _player2 );
      }
    }
  }
  else if ( _waitStart )
  {
    if ( GetAsyncKeyState( 0x52 ) ) restartMatch();  // r
  }
}

void Game::checkEndOfGame()
{
  ScoreBoard* board = ( (ScoreBoard*)_elements[_indexUi].get() );
  if ( _gameFinished && !_waitStart )
  {
    playMusicEndMatch();
  }
  else if ( !_gameFinished )
  {
    if ( _player1->getScore() >= _nbRound )
    {
      board->setMessage( "Player 1 win !! Press R to Restart." );
      _elements[_indexPuck]->getPhysicComponent()->setPosition(
          Eigen::Vector2f( SCREEN_WIDTH / 2, SCREEN_HEIGHT / 2 + SCORE_BOARD_HEIGHT ) );
      _gameFinished = true;
    }
    else if ( _player2->getScore() >= _nbRound )
    {
      board->setMessage( "Player 2 win !! Press R to Restart." );
      _elements[_indexPuck]->getPhysicComponent()->setPosition(
          Eigen::Vector2f( SCREEN_WIDTH / 2, SCREEN_HEIGHT / 2 + SCORE_BOARD_HEIGHT ) );
      _gameFinished = true;
    }
  }
}

Game& Game::getInstance()
{
  static Game instance;

  return instance;
}

void Game::mainLoop()
{
  playMusicBeginMatch();

  while ( true )
  {
    // Escape
    if ( GetAsyncKeyState( 27 ) ) break;

    // Input
    checkInputs();

    // Physic
    physicStep();

    // Logic
    for ( std::unique_ptr<GameElement>& el : _elements )
    {
      if ( el->isAlive() ) el->update( _deltaTime );
    }
    checkAliveElements();
    checkEndOfGame();

    // Render
    Sleep( 1 );
    ( (ScoreBoard*)_elements[_indexUi].get() )->setScore( _player1, _player2 );
    window.clear();
    for ( std::unique_ptr<GameElement>& el : _elements )
    {
      if ( el->isAlive() ) el->getGraphicComponent()->display( window );
    }
    window.render();

    Sleep( _deltaTime * 1000 );
  }
  window.clear();
  while ( _threadActive )
    ;
  // Exit
  playMusicEndMatch();
  window.render();
}

void Game::physicStep()
{
  //-> Positions
  for ( std::unique_ptr<GameElement>& el : _elements )
  {
    if ( el->isAlive() ) el->getPhysicComponent()->computePosition( _deltaTime );
  }

  //-> Collisions
  for ( std::unique_ptr<GameElement>& el1 : _elements )
  {
    for ( std::unique_ptr<GameElement>& el2 : _elements )
    {
      if ( el1 != el2 && el1->isAlive() && el2->isAlive() )
      {
        el1->getPhysicComponent()->collideWith( el2->getPhysicComponent() );
      }
    }
  }
}

void Game::restartRound( Player* playerWhoScore )
{
  int puckPlace;

  if ( playerWhoScore->getNoPlayer() == 1 )
  {
    puckPlace = 20;
  }
  else
  {
    puckPlace = -20;
  }
  _elements[_indexPuck]->getPhysicComponent()->setPosition(
      Eigen::Vector2f( SCREEN_WIDTH / 2 + puckPlace, SCREEN_HEIGHT / 2 + SCORE_BOARD_HEIGHT ) );
  _elements[_indexPuck]->getPhysicComponent()->setVelocity( Eigen::Vector2f( 0.0f, 0.0f ) );
  _elements[_indexPuck]->getPhysicComponent()->setForce( Eigen::Vector2f( 0.0f, 0.0f ) );

  _player1->getPhysicComponent()->setPosition(
      Eigen::Vector2f( SCREEN_WIDTH / 4, SCREEN_HEIGHT / 2 + SCORE_BOARD_HEIGHT ) );
  _player1->getPhysicComponent()->resetCollisions();

  _player2->getPhysicComponent()->setPosition(
      Eigen::Vector2f( SCREEN_WIDTH * 0.75f, SCREEN_HEIGHT / 2 + SCORE_BOARD_HEIGHT ) );
  _player2->getPhysicComponent()->resetCollisions();

  Beep( 280, 300 );
  Beep( 170, 100 );
  Beep( 260, 800 );
}

void Game::restartMatch()
{
  _gameFinished = true;
  _waitStart = true;

  _elements[_indexPuck]->getPhysicComponent()->setPosition(
      Eigen::Vector2f( SCREEN_WIDTH / 2, SCREEN_HEIGHT / 2 + SCORE_BOARD_HEIGHT ) );
  _elements[_indexPuck]->getPhysicComponent()->setVelocity( Eigen::Vector2f( 0.0f, 0.0f ) );
  _elements[_indexPuck]->getPhysicComponent()->setForce( Eigen::Vector2f( 0.0f, 0.0f ) );

  _player1->getPhysicComponent()->setPosition(
      Eigen::Vector2f( SCREEN_WIDTH / 4, SCREEN_HEIGHT / 2 + SCORE_BOARD_HEIGHT ) );
  _player1->getPhysicComponent()->resetCollisions();

  _player2->getPhysicComponent()->setPosition(
      Eigen::Vector2f( SCREEN_WIDTH * 0.75f, SCREEN_HEIGHT / 2 + SCORE_BOARD_HEIGHT ) );
  _player2->getPhysicComponent()->resetCollisions();

  _player1->resetTimer();
  _player2->resetTimer();
  _player1->setScore( 0 );
  _player2->setScore( 0 );

  for ( int i = _indexBonusWalls; i < _elements.size(); i++ )
  {
    _elements[i]->setAlive( false );
  }

  playMusicBeginMatch();
}

void Game::playMusicBeginMatch()
{
  if ( !_threadActive )
  {
    std::thread startMatch(
        [&]()
        {
          _threadActive = true;
          ScoreBoard* board = ( (ScoreBoard*)_elements[_indexUi].get() );
          board->setMessage( "Game Begin in 3..." );
          Beep( 220, 300 );
          Beep( 294, 300 );
          board->setMessage( "Game Begin in 2..." );
          Beep( 294, 300 );
          Beep( 370, 300 );
          board->setMessage( "Game Begin in 1..." );
          Beep( 494, 300 );
          Beep( 370, 300 );
          Beep( 440, 800 );
          board->setMessage( "Go" );
          _gameFinished = false;
          _waitStart = false;
          _threadActive = false;
          Sleep( 3000 );
          if ( !_threadActive ) board->setMessage( "" );
        } );
    startMatch.detach();
  }
}

void Game::playMusicEndMatch()
{
  Beep( 440, 200 );
  Beep( 495, 200 );
  Beep( 260, 200 );
  Beep( 586, 200 );
  Beep( 330, 200 );
  Beep( 260, 200 );
  Beep( 195, 200 );
  Beep( 260, 400 );
  Beep( 293, 600 );
  Beep( 440, 1000 );
  _waitStart = true;
}
